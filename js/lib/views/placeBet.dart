import 'package:betk/controller/BetController.dart';
import 'package:betk/views/OddsPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'navigationDrawer.dart';

class PlaceBet extends StatefulWidget {

  @override
  _PlaceBetState createState() => _PlaceBetState();
}

class _PlaceBetState extends State<PlaceBet> {
  var betController=Get.find<BetController>();
  var betStyle=TextStyle(
    fontSize: 20,
    color: Colors.white,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black12,
        centerTitle: true,
        title: Text(
          "BetX.Com",
          style: TextStyle(
              color: Colors.black, fontSize: 30, fontWeight: FontWeight.w800),
        ),
      ),
      drawer:Container(
        width: MediaQuery.of(context).size.width*0.7,
        child: NavigationDrawer(),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue,
        child:

            Text("Terms And conditions apply",
            style:TextStyle(
            fontSize: 16,
            fontStyle: FontStyle.italic,
            color: Colors.black,
            )),



        elevation: 0,
      ),
      backgroundColor: Colors.tealAccent,
      body: ListView(
        children: [
          Container(
              height: MediaQuery.of(context).size.height * 0.6,
            child: ListView.builder(
                itemCount: betController.allOdds.length,
                itemBuilder: (context,index){
                  return  Container(

                    margin: EdgeInsets.all(2),
                    padding: EdgeInsets.all(5),
                    color: Colors.white,
                    child: ListTile(
                      title: Text("${betController.allIdOdds[index]['homeName'] } "
                          "${betController.allIdOdds[index]['drawName'] }"
                          "${betController.allIdOdds[index]['awayName'] }",
                          style:TextStyle(
                            fontSize: 12,
                            color: Colors.black,)),
                      subtitle: Text("You Pick ${betController.allOdds[index]}",
                          style:TextStyle(
                            fontSize: 16,
                            color: Colors.green,)),
                      trailing:Icon(Icons.delete),
                      onTap: (){
                        betController.allOdds.removeAt(index);
                        betController.allIdOdds.removeAt(index);
                        setState(() {

                        });
                        Get.snackbar("Event deleted", "",snackPosition: SnackPosition.BOTTOM,backgroundColor:Colors.red,colorText: Colors.white);
                      },
                        ),
                  );


                }
            )
            ),
          Container(
            color: Colors.grey,
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: [
                      Expanded(
                          child: Text("Yor Balance",style: betStyle,)),
                      Expanded(
                          child: Text("${betController.balance}",style: betStyle))
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: [
                      Expanded(
                          child: Text("Total Odds",style: betStyle,)),
                      Expanded(
                          child: Text("${betController.sum.toPrecision(3)}",style: betStyle))
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                        child: Text("Enter Stake",style: betStyle,)),
                    Expanded(
                        child: TextField(
                          keyboardType: TextInputType.number,
                          controller: betController.stakeController,

                          decoration: InputDecoration(
                            hintText: "min 20.0",
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.white
                                )
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blue
                                )
                            )
                          ),
                        )
                    ),
                    Expanded(
                      // ignore: deprecated_member_use
                      child: Container(
                        margin: EdgeInsets.only(left: 10),
                        // ignore: deprecated_member_use
                        child: RaisedButton(
                          child: Text("OK"),
                          color: Colors.red,
                          onPressed: (){
                            betController.posibleWin();
                            setState(() {

                            });
                            Get.snackbar("Bet Placed Succesfully", "",snackPosition: SnackPosition.TOP,backgroundColor:Colors.green,colorText: Colors.white);

                          },
                        ),
                      ),
                    )
                  ]
                ),
                Row(
                  children: [
                    Expanded(
                        child: Text("Posiible win",style: betStyle,)),
                    Expanded(
                        child: Text("${betController.totalWin.toPrecision(2)}",style: betStyle))
                  ],
                ),

              ],
            ),
          )
        ],
      ),

      );

  }
}
