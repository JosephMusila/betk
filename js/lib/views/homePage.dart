import 'package:betk/widgets/customWidgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'navigationDrawer.dart';

class HomePage extends StatelessWidget {
  var widgetController=Get.put(Custom());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black12,
        centerTitle: true,

        title:  Text("BetX.Com",style:
        TextStyle(
            color: Colors.green,
            fontSize: 30,
            fontWeight: FontWeight.w800
        )
          ,),
      ),
      drawer:NavigationDrawer(),
      backgroundColor: Colors.black12,
      body: Container(),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){

        },
        backgroundColor: Colors.blue,
        label: Text("Betslip: 0"),
      ),
    );

  }
}
