import 'package:betk/controller/BetController.dart';
import 'package:betk/views/placeBet.dart';
import 'package:betk/widgets/customWidgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:convert';

import 'navigationDrawer.dart';

class OddsPage extends StatefulWidget {
  const OddsPage({Key? key}) : super(key: key);

  @override
  _OddsPageState createState() => _OddsPageState();
}

class _OddsPageState extends State<OddsPage> {
  var widgetController=Get.put(Custom());
  var betController=Get.put(BetController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black12,
        centerTitle: true,
        title: Text(
          "BetX.Com",
          style: TextStyle(
              color: Colors.green, fontSize: 30, fontWeight: FontWeight.w800),
        ),
      ),
      drawer:Container(
        width: MediaQuery.of(context).size.width*0.7,
        child: NavigationDrawer(),
      ),
      backgroundColor: Colors.black12,
      body: FutureBuilder(
        future:
            DefaultAssetBundle.of(context).loadString('assets/jsonOdss.json'),
        builder: (context, snapshot) {
         var showData = json.decode(snapshot .data .toString());

          return ListView.builder(

              itemBuilder: (
                  context, index) {
                return Column(children: [
                  Text(showData[index]['id'],
                      style: TextStyle(
                      fontSize: 12,
                        color: Colors.white,
                      )),
                  Row(children: [
                    Expanded(
                      child: Column(
                        children: [
                          Text(
                            showData[index]['homeName'].toString().toUpperCase(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                          // ignore: deprecated_member_use
                          RaisedButton(
                            onPressed: () {
                              betController.chosenId=showData[index];
                              betController.chosenOddIndex=int.parse(showData[index]['id']);
                              betController.chosenOdd=double.parse(showData[index]['homeOdds'].toString());
                              betController.addOdds();
                            },
                            color: Colors.amber,
                            highlightColor: Colors.blue,
                            textColor: Colors.black,
                            child: Text(showData[index]['homeOdds'].toString(),
                              style: TextStyle(
                                  fontSize: 14
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Text(
                            showData[index]['drawName'],
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                          // ignore: deprecated_member_use
                          RaisedButton(
                            onPressed: () {
                              betController.chosenOddIndex=int.parse(showData[index]['id']);
                              betController.chosenOdd=double.parse(showData[index]['drawOdds'].toString());
                              betController.addOdds();
                            },
                            color: Colors.amber,
                            textColor: Colors.black,
                            child: Text(showData[index]['drawOdds'].toString()),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Text(

                            showData[index]['awayName'].toString().toUpperCase(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                          // ignore: deprecated_member_use
                          RaisedButton(
                            onPressed: () {
                              betController. chosenOddIndex=int.parse(showData[index]['id']);
                              betController.chosenOdd=double.parse(showData[index]['awayOdds'].toString());
                              betController.addOdds();
                            },
                            color: Colors.amber,

                            textColor: Colors.black,
                            child: Text(showData[index]['awayOdds'].toString().toUpperCase(),
                            style: TextStyle(
                              fontSize: 14
                            ),
                            ),
                          )
                        ],
                      ),
                    ),

                  ]),
                  widgetController.divider(Colors.white),
                ]);
              },
                 itemCount: showData.length,
              );
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){
          Get.to(PlaceBet());
        },
        backgroundColor: Colors.blue,
        label: Obx(()=>
         Text("Events on betslip :${betController.betSlipCount}")

        )
      ),
    );
  }
}
