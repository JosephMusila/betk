import 'package:betk/controller/BetController.dart';
import 'package:betk/views/OddsPage.dart';
import 'package:betk/views/placeBet.dart';
import 'package:betk/widgets/customWidgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NavigationDrawer extends StatelessWidget {
  var widgetController=Get.find<Custom>();
  var betController=Get.find<BetController>();
  var style=TextStyle(
    fontSize: 20,
    color: Colors.white,

  );
  @override
  Widget build(BuildContext context) {
    return Drawer(

      elevation: 5.0,
      child: Material(
        color: Colors.blueAccent,
        shadowColor: Colors.black,

        child: ListView(
          children: [
            Center(child: Text("Main Menu",style: TextStyle(
              color: Colors.black,
              fontSize: 14,

            ),)),
            widgetController.divider(Colors.white),
            Container(

              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("User Name : Musila",style: style,),
                  Text("Email : josemusila03@gmail.com",style: style,),
                  Text("Balance : ${betController.balance}",style: style,),
                  Text("Bonus : 0.00",style: style,),
                ],
              ),
            ),
            widgetController.divider(Colors.white),
            meniItem(text: "Home", icon: Icons.home,
            onClicked: ()=>SelectedItem(context,0),
            ),
            meniItem(text: "BetSlip", icon: Icons.article,
              onClicked: ()=>SelectedItem(context,1),
            )
          ],
        ),
      ),
    );
  }

  Widget meniItem({
  required String text,
  required IconData icon,
    VoidCallback?onClicked
  }){
    final color=Colors.white;
    final hoverColor=Colors.white;
    return ListTile(
      leading: Icon(icon,color:color),
      title: Text(text,style:TextStyle(color:color)),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }
  void SelectedItem(context,index){
    switch(index){
      case 0:
        Get.to(OddsPage());
        break;
      case 1:
        Get.to(PlaceBet());
        break;
    }
  }
}
