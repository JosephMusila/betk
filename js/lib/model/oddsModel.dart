class OddsValue {
 late List<Matches> matches;

  OddsValue({required this.matches});

  OddsValue.fromJson(Map<String, dynamic> json) {
    if (json['matches'] != null) {
      matches =  <Matches>[];
      json['matches'].forEach((v) {
        matches.add(new Matches.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.matches != null) {
      data['matches'] = this.matches.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Matches {
  late String id;
  late String homeName;
  late double homeOdds;
  late String drawName;
  late int drawOdds;
  late String awayName;
  late int awayOdds;

  Matches(
      {
        required this.id,
        required this.homeName,
        required this.homeOdds,
        required this.drawName,
        required this.drawOdds,
        required this.awayName,
        required this.awayOdds});

  Matches.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    homeName = json['homeName'];
    homeOdds = json['homeOdds'];
    drawName = json['drawName'];
    drawOdds = json['drawOdds'];
    awayName = json['awayName'];
    awayOdds = json['awayOdds'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['homeName'] = this.homeName;
    data['homeOdds'] = this.homeOdds;
    data['drawName'] = this.drawName;
    data['drawOdds'] = this.drawOdds;
    data['awayName'] = this.awayName;
    data['awayOdds'] = this.awayOdds;
    return data;
  }
}
