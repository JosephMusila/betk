import 'dart:convert';
import 'dart:math';

import 'package:betk/model/jsonManager.dart';
import 'package:betk/model/oddsModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class Custom extends  GetxController{






  Widget divider(Color color){
    return Container(
      child: Divider(
        thickness: 3,
        color: color,

      ),
    );
  }
  Widget terms(){
    return Container(
      height: 150,
      color: Colors.tealAccent,
       padding: EdgeInsets.all(10),
       child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Contacts",style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),),
                Text("0745787487",style: TextStyle(
                  fontSize: 14,

                ),),

                Text("0759836464",style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                ),),
              ],
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Terms of service",style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),),
                Text("www.betx.com",style: TextStyle(
                  fontSize: 14,
                  color: Colors.blue,
                  fontStyle: FontStyle.italic
                ),),

                Text("info@betx.co.ke",style: TextStyle(
                  fontSize: 14,
                    color: Colors.blue,
                    fontStyle: FontStyle.italic
                ),),
              ],
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Offices",style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),),
                Text("Nairobi- Anniversary Towers",style: TextStyle(
                    fontSize: 14,
                    color: Colors.blue,

                ),),

                Text("Tanzania- Dar es salaam",style: TextStyle(
                    fontSize: 14,
                    color: Colors.blue,

                ),),
              ],
            ),
          )
        ],
      ),
    );
  }
}